Логическая структура любого алгоритма может быть представлена комбинацией трех базовых структур: следование (образуется последовательностью действий, следующих одно за другим), ветвление, цикл (обеспечивает многократное выполнение некоторой совокупности действий, которая называется телом цикла).<br>
Базовая структура – ветвление.<br>
Обеспечивает в зависимости от результата проверки условия (да или нет) выбор одного из альтернативных путей работы алгоритма. Каждый из путей ведет к общему выходу, так что работа алгоритма будет продолжаться независимо от того, какой путь будет выбран.<br>
Условный оператор IF . Позволяет проверить некоторое условие и в зависимости от результатов проверки выполнить то или иное действие. Таким образом, условный оператор – это средство ветвления вычислительного процесса.<br>
IF <логическое_условие> THEN оператор_1 ELSE оператор_2;<br>
Часть ELSE оператор_2 условного оператора может быть опущена, тогда получается краткая форма записи оператора IF.<br>
IF <логическое_условие> THEN оператор;<br>
Тогда при выполнении условия выполняется оператор, в противном случае он пропускается.<br>
Действие условного оператора можно расширить, если использовать составной оператор.<br>
IF <логическое_условие> THEN<br>
BEGIN<br>
операторы<br>
END<br>
ELSE<br>
BEGIN<br>
операторы<br>
END;<br><br>
<b>Логические выражения</b><br>
1)знаки отношений <,>,=,<=,>=,<>;<br>
2)логические операции: NOT (отрицание – НЕ), AND (умножение - И), OR (сложение – ИЛИ), XOR (исключение - ИЛИ).<br>
Оператор выбора CASE . Позволяет выбрать одно из нескольких возможных продолжений программы. Параметром по которому осуществляется выбор, служит ключ выбора – переменная или выражение (кроме типов REAL и STRING). Ключ выбора – порядковый тип или CHAR.<br>
CASE <ключ_выбора> OF<br>
константы:операторы<br>
END;<br>
Или<br>
CASE <ключ_выбора> OF<br>
константы:операторы<br>
ELSE оператор<br>
END;<br>
<b>Любому из операторов списка выбора может предшествовать не одна, а несколько констант выбора, разделенных запятыми.</b><br><br>
Program primer_IF;<br>
VAR x,y,max:integer;<br>
BEGIN<br>
Writeln (‘Vvedite 2 chisla’);<br>
Readln(x,y);<br>
IF x>=y then max:=x ELSE max:=y;<br>
Writeln (‘Max=’,max);<br>
END.<br><br>
Program primer_CASE;<br>
VAR i:integer;<br>
BEGIN<br>
Writeln (‘Vvedite nomer kvartiri ot 1 do 5’);<br>
Readln (i);<br>
CASE I OF<br>
1:writeln(‘Ivanovi’);<br>
2:writeln(‘Petrovi’);<br>
3:writeln(‘Zubakovi’);<br>
4:writeln(‘Lisicina’);<br>
5:writeln(‘Koshkina’);<br>
ELSE writeln (‘kvartir bolshe net’);<br>
End;<br>
End.<br><br>
Используя несколько операторов IF, можно производить ветвление по последовательности условий, но это очень утомительно. Оператор выбора CASE позволяет выбрать одно из нескольких возможных продолжений программы, что наиболее удобно. Параметром, по которому осуществляется выбор, служит так называемый ключ выбора.<br>
8+9. Логическая структура любого алгоритма может быть представлена комбинацией трех базовых структур: следование (образуется последовательностью действий, следующих одно за другим), ветвление (обеспечивает в зависимости от результата проверки условия (да или нет) выбор одного из альтернативных путей работы алгоритма), цикл.<br>
<b>Базовая структура – цикл.</b><br>
Цикл – многократно повторяющийся участок вычислительного процесса.<br>
Циклы: арифметические (заранее известно количество необходимых повторений, FOR), итерационные (количество необходимых повторений заранее неизвестно). Итерационные: цикл с предусловием (WHILE) и цикл с постусловием (REPEAT).<br>
Оператор FOR (цикл с параметром).<br><br>
FOR i:=n1 TO n2 DO <оператор>;<br>
n1 – начальное значение, n2 – конечное значение.<br>
Данный цикл предназначен для организации циклов, когда заранее известно сколько раз должно повториться тело цикла.<br>
Работа оператора FOR:<br><br>
1) <i>вычисляются начальное и конечное значения;</i><br>
2) <i>параметр цикла получает начальное значение;</i><br>
3) <i>проверяется условие : параметр цикла <= конечное значение;</i><br>
4) <i>если условие истинно, то выполняется тело цикла, в противном случае работа цикла прекращается;</i><br>
5) <i>параметр цикла увеличивается на 1;</i><br>
6) <i>переходим к пункту 3.</i><br><br>
Итерационные циклы.<br>
Оператор WHILE (ЦИКЛ с предусловием).<br>
WHILE <условие> DO <оператор>;<br><br>
Выполнение оператора цикла начинается с проверки условия, записанного после слова WHILE, если оно выполняется, то выполняется тело цикла, а затем вновь идет проверка условия. Как только на очередном шаге окажется, что выполнение не соблюдается, выполнение цикла прекращается. Условие вычисляется и анализируется перед каждым выполнением цикла (отсюда и берется термин предусловия).Рекомендации:<br>
1) <i>в условии, как в логическом выражении, должны обязательно фигурировать переменные, изменяющие свои значения в теле цикла;</i><br><br>
2) <i>во избежание зацикливания лучше сначала написать условие прекращения цикла и взять потом его отрицание;</i><br>
3) <i>переменные логического выражения должны получить свои исходные значения до входа в оператор WHILE.</i><br><br>
Оператор REPEAT (цикл с постусловием).<br>
REPEAT <тело_цикла> UNTIL <условие>;<br>
Оператор REPEAT называется оператором цикла с постусловием, т.к. здесь выражение, управляющее повторным циклом, размещается после тела цикла. Последовательность операторов, входящих в тело цикла, выполняется один раз, после чего идет проверка условия, записанного за служебным словом UNTIL. Если условие соблюдается – цикл завершается, в противном случае тело цикла повторяется еще раз, после чего снова идет проверка условия.<br>
! В операторе REPEAT проверка условия выхода из цикла выполняется в конце, а не в начале цикла, как в операторе WHILE. Поэтому в операторе REPEAT тело цикла выполняется хотя бы один раз. В операторе REPEAT выход из цикла осуществляется по истинности условия, а в WHILE – по ложности.<br>
Program with_for;<br><br>
VAR x,y:integer;<br>
BEGIN<br>
x:=5<br>
FOR y:=2 TO 10 DO<br>
x:=x+2;<br>
WRITELN(x);<br>
READLN;<br>
END.<br><br>
PROGRAM WITH_WHILE;<br>
VAR a,b:ineger;<br>
BEGIN<br>
a:=2; b:=3;<br>
WHILE b<=15 DO<br>
BEGIN<br>
a:=a+2; b:=b+3;<br>
END;<br>
WRITELN (a); READLN;<br>
END.<br><br>
PROGRAM WITH_repeat;<br>
VAR a,b:ineger;<br>
BEGIN<br>
a:=2; b:=3;<br>
repeat a:=a+2; b:=b+3;<br>
until b<=15;<br>
WRITELN (a)READLN;<br>
END.