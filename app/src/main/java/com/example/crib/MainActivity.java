package com.example.crib;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ListView listView =  findViewById(R.id.listView);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,

                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.menuItems));

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override

            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {

                Intent intent = new Intent(MainActivity.this, ViewActivity.class);

                intent.putExtra("id", id);

                startActivity(intent);
            }
        });
    }
}