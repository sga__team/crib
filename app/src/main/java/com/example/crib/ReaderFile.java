package com.example.crib;

import android.app.Activity;
import android.content.res.AssetManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

 class ReaderFile extends Activity {

        /*
         * Метод получения строки из файла.
         *
         * Метод на вход получает имя файла, о нужной пользователю информацией и преобразует
         * его в строку, используя доспут к Asset от AssetManager.
         *
         * @fileName - имя файла
         * @return - строку с текстом полученную из файла
         */
        static String getStringFromAssetFile(Activity activity, String fileName) throws IOException {

            AssetManager assetManager = activity.getAssets();

            InputStream inputStream = assetManager.open(fileName);

            String string = convertStreamToString(inputStream);

            inputStream.close();

            return string;
        }

        /*
         * Метод конвертирующий поток в строку.
         *
         * На вход получает поток, который преобразуется в строку, если же строку не удалось
         * преобразовать, то выведет сообщение об ошибке.
         *
         * @inputStream - поток со строкой
         * @return - возвращает строку
         */
    private static String convertStreamToString(InputStream inputStream)  {

        if (inputStream != null) {

            StringBuilder stringBuilder = new StringBuilder();

            String line;

            try(BufferedReader bufferedReader = new BufferedReader(

                    new InputStreamReader(inputStream));) {

                while ((line = bufferedReader.readLine()) != null) {

                    stringBuilder.append(line).append("\n");

                }

            } catch (IOException e) {

                e.printStackTrace();

            }

            return stringBuilder.toString();

        } else {

            return "К сожалению, считывание файла не удалось";

        }
    }
}
