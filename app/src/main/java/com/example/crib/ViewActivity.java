package com.example.crib;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;
import java.io.IOException;

public class ViewActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view);

        TextView text = findViewById(R.id.textView);

        Intent intent = getIntent();

        int id = (int)intent.getLongExtra("id", 0);//получаем Id файла

        String htmlText = null;//переменная для текста прочитанноготиз файла

        String[] arrayNameFile = getResources().getStringArray(R.array.fileNames);//получаем из ресурсов массив строк с названиями файлов

        String fileName = arrayNameFile[id];//по id находим нужный файл

        try {

            htmlText = ReaderFile.getStringFromAssetFile(this, fileName);//пробуем чтитать

        } catch (IOException e) {

            e.printStackTrace();

        }

        text.setText(Html.fromHtml(htmlText));//печатаем
    }

}
